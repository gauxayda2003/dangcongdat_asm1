﻿// See https://aka.ms/new-console-template for more information
using System;
using System.Text;

namespace GreatestCommonDivisor
{
    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;

            // Yêu cầu người dùng nhập hai số nguyên
            Console.Write("Nhập số thứ nhất: ");
            int number1 = int.Parse(Console.ReadLine());

            Console.Write("Nhập số thứ hai: ");
            int number2 = int.Parse(Console.ReadLine());


            int gcd = TimUocSoChungLonNhat(number1, number2);
            Console.WriteLine($"Ước số chung lớn nhất của {number1} và {number2} là: {gcd}"); // Tìm và in ra ước số chung lớn nhất
        }

        static int TimUocSoChungLonNhat(int a, int b)
        {
            // Chuyển đổi a và b thành giá trị tuyệt đối
            a = Math.Abs(a);
            b = Math.Abs(b);

            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }

            return a;
        }
    }
}

