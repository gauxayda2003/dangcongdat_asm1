﻿// See https://aka.ms/new-console-template for more information
using System;
using System.Text;

namespace GreatestCommonDivisorArray
{
    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            // Yêu cầu người dùng nhập các số nguyên vào mảng
            Console.Write("Nhập số lượng số nguyên trong mảng: ");
            int n = int.Parse(Console.ReadLine());

            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                Console.Write($"Nhập số nguyên thứ {i + 1}: ");
                arr[i] = int.Parse(Console.ReadLine());
            }

            // Tìm và in ra ước số chung lớn nhất của mảng
            int gcd = TimUocSoChungLonNhatMang(arr);
            Console.WriteLine($"Ước số chung lớn nhất của mảng là: {gcd}");
        }

        static int TimUocSoChungLonNhat(int a, int b)
        {
            a = Math.Abs(a);
            b = Math.Abs(b);

            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }

            return a;
        }

        static int TimUocSoChungLonNhatMang(int[] arr)
        {
            if (arr.Length == 0)
            {
                return 0; // Trường hợp mảng rỗng
            }

            int gcd = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                gcd = TimUocSoChungLonNhat(gcd, arr[i]);
            }

            return gcd;
        }
    }
}

