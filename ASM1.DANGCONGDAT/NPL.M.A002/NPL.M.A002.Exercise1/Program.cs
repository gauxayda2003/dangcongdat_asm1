﻿// See https://aka.ms/new-console-template for more information
using System;
using System.Text;

namespace MaxMinArray
{
    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;

            Console.Write("Nhập số lượng giá trị trong dãy: ");
            int n = int.Parse(Console.ReadLine());

            if (n <= 0)
            {
                Console.WriteLine("Số lượng giá trị không hợp lệ.");
                return;
            }

            int[] array = new int[n];

            for (int i = 0; i < n; i++)
            {
                Console.Write($"Nhập giá trị thứ {i + 1}: ");
                array[i] = int.Parse(Console.ReadLine());
            }

            int max = array[0];
            int min = array[0];

            for (int i = 1; i < n; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }

                if (array[i] < min)
                {
                    min = array[i];
                }
            }

            Console.WriteLine($"Giá trị lớn nhất trong dãy là: {max}");
            Console.WriteLine($"Giá trị nhỏ nhất trong dãy là: {min}");
        }
    }
}

