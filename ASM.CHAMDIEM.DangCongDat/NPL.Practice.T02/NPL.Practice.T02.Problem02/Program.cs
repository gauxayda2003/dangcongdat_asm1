﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;

        int arrayLength, subLength;

        // Nhập độ dài của mảng
        do
        {
            Console.Write("Nhập độ dài của mảng: ");
        }
        while (!int.TryParse(Console.ReadLine(), out arrayLength) || arrayLength <= 0);

        int[] inputArray = new int[arrayLength];

        // Nhập các phần tử của mảng
        for (int i = 0; i < arrayLength; i++)
        {
            do
            {
                Console.Write("arr[" + (i + 1) + "]: ");
            }
            while (!int.TryParse(Console.ReadLine(), out inputArray[i]));
        }

        // Nhập độ dài của dãy con
        do
        {
            Console.Write("Nhập độ dài của dãy con: ");
        }
        while (!int.TryParse(Console.ReadLine(), out subLength) || subLength <= 0);

        int result = FindMaxSubArray(inputArray, subLength);
        Console.WriteLine("Tổng lớn nhất của các phần tử trong dãy con: " + result);
    }

    static int FindMaxSubArray(int[] inputArray, int subLength)
    {
        int maxSum = 0;
        int currentSum = 0;

        // Tính tổng các phần tử trong dãy con ban đầu
        for (int i = 0; i < subLength; i++)
        {
            currentSum += inputArray[i];
        }

        maxSum = currentSum;

        // Duyệt qua mảng để tìm tổng lớn nhất của dãy con
        for (int i = subLength; i < inputArray.Length; i++)
        {
            currentSum -= inputArray[i - subLength];
            currentSum += inputArray[i];
            maxSum = Math.Max(maxSum, currentSum);
        }

        return maxSum;
    }
}
