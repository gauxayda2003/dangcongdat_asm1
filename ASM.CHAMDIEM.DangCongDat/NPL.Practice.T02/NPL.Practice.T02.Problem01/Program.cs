﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.WriteLine("Nhập nội dung bài viết:");
        string contentOfArticle = Console.ReadLine();

        int maxLength;
        while (true)
        {
            try
            {
                Console.WriteLine("Nhập số ký tự tối đa cho tóm tắt:");
                maxLength = int.Parse(Console.ReadLine());
                break;
            }
            catch (Exception e)
            {
                Console.WriteLine("Vui lòng nhập lại.");
            }
        }

        string summary = GetArticleSummary(contentOfArticle, maxLength);
        Console.WriteLine(summary);
    }

    static string GetArticleSummary(string contentOfArticle, int maxLength)
    {
        if (maxLength > contentOfArticle.Length)
        {
            // Trường hợp chuỗi ban đầu đã ngắn hơn độ dài tối đa, không cần cắt.
            return contentOfArticle.Trim();
        }

        int index = maxLength - 1;

        if (contentOfArticle[index] == ' ')
        {
            // Nếu ký tự cuối cùng là khoảng trắng, cắt ngay tại vị trí đó và thêm "...".
            return contentOfArticle.Substring(0, index).Trim() + "...";
        }

        // Tìm vị trí khoảng trắng gần nhất trước vị trí maxLength để cắt.
        while (index >= 0 && contentOfArticle[index] != ' ')
        {
            index--;
        }

        if (index >= 0)
        {
            return contentOfArticle.Substring(0, index).Trim() + "...";
        }

        // Nếu không tìm thấy khoảng trắng, cắt ngay tại vị trí maxLength.
        return contentOfArticle.Substring(0, maxLength).Trim() + "...";
    }
}
