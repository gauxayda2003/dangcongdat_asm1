﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise4
{
    public static class ArrayExtensions
    {
        // Loại bỏ các phần tử trùng lặp trong mảng
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            List<T> distinctList = new List<T>();

            foreach (T item in array)
            {
                if (!distinctList.Contains(item))
                {
                    distinctList.Add(item);
                }
            }

            return distinctList.ToArray();
        }

        // Trả về phần tử lớn thứ hai trong mảng
        public static T SecondLargest<T>(this T[] array) where T : IComparable<T>
        {
            if (array.Length < 2)
            {
                throw new InvalidOperationException("Mảng phải có ít nhất 2 phần tử.");
            }

            var distinctArray = array.RemoveDuplicate();
            if (distinctArray.Length < 2)
            {
                throw new InvalidOperationException("Không có phần tử lớn thứ hai trong mảng.");
            }

            return distinctArray.OrderByDescending(x => x).ElementAt(1);
        }

        // Trả về phần tử lớn thứ order trong mảng
        public static T OrderLargest<T>(this T[] array, int order) where T : IComparable<T>
        {
            if (order < 1 || order > array.Length)
            {
                throw new ArgumentException("Thứ tự không hợp lệ.");
            }

            var distinctArray = array.RemoveDuplicate();
            if (distinctArray.Length < order)
            {
                throw new InvalidOperationException("Không có đủ phần tử cho thứ tự đã cho.");
            }

            return distinctArray.OrderByDescending(x => x).ElementAt(order - 1);
        }
    }
}
