﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.M.A011.Exercise3
{
    public static class ArrayExtensions
    {
        // Trả về chỉ số cuối cùng của phần tử có giá trị bằng elementValue trong mảng.
        public static int LastIndexOf<T>(this T[] array, T elementValue)
        {
            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (object.Equals(array[i], elementValue))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
