﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Net.M.A011.Exercise2
{
    public static class ArrayExtensions
    {
        // Trả về mảng chứa các phần tử duy nhất từ mảng số nguyên
        public static int[] RemoveDuplicate(this int[] array)
        {
            var distinctList = new List<int>();

            foreach (int item in array)
            {
                if (!distinctList.Contains(item))
                {
                    distinctList.Add(item);
                }
            }

            return distinctList.ToArray();
        }

        // Trả về mảng chứa các phần tử duy nhất từ mảng bất kỳ kiểu dữ liệu nào
        public static T[] RemoveDuplicate<T>(this T[] array)
        {
            var distinctList = new List<T>();

            foreach (T item in array)
            {
                if (!distinctList.Contains(item))
                {
                    distinctList.Add(item);
                }
            }

            return distinctList.ToArray();
        }
    }
}
