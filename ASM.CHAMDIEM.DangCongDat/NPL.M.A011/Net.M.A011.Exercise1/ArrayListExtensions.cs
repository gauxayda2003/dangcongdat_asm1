﻿using System;
using System.Collections;
using System.Linq;

namespace Net.M.A011.Exercise1
{
    public static class ArrayListExtensions
    {
        // Đếm số lượng phần tử kiểu int trong một ArrayList
        public static int CountInt(this ArrayList array)
        {
            return array.OfType<int>().Count();
        }

        // Đếm số lượng phần tử có kiểu dữ liệu là dataType trong ArrayList
        public static int CountOf(this ArrayList array, Type dataType)
        {
            return array.Cast<object>().Count(item => item.GetType() == dataType);
        }

        // Đếm số lượng phần tử có kiểu dữ liệu là T trong ArrayList
        public static int CountOf<T>(this ArrayList array)
        {
            return array.OfType<T>().Count();
        }

        // Tìm giá trị lớn nhất kiểu T trong ArrayList (nếu T là kiểu số)
        public static T MaxOf<T>(this ArrayList array)
        {
            if (IsNumericType(typeof(T)))
            {
                var numericList = array.OfType<T>().ToList();
                if (numericList.Any())
                {
                    return numericList.Max();
                }
                else
                {
                    throw new InvalidOperationException("Không tìm thấy phần tử kiểu số cụ thể.");
                }
            }
            else
            {
                throw new InvalidOperationException("T phải là kiểu dữ liệu số.");
            }
        }

        // Kiểm tra xem kiểu dữ liệu có phải là kiểu số không
        private static bool IsNumericType(Type type)
        {
            return type == typeof(int) || type == typeof(double) || type == typeof(float) || type == typeof(long);
        }
    }
}
