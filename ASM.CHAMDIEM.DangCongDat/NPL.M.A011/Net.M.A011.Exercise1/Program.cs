﻿using Net.M.A011.Exercise1;
using System.Collections;
using System.ComponentModel.DataAnnotations;



ArrayList arr = new ArrayList();
arr.Add("Hieu");
arr.Add("Hoang");
arr.Add(1);
arr.Add(2);
arr.Add("Trong");
arr.Add(3.9d);
Console.WriteLine(ArrayListExtensions.CountInt(arr));
Console.WriteLine(ArrayListExtensions.CountOf(arr, typeof(int)));
Console.WriteLine(ArrayListExtensions.CountOf<string>(arr));
Console.WriteLine(ArrayListExtensions.MaxOf<double>(arr));