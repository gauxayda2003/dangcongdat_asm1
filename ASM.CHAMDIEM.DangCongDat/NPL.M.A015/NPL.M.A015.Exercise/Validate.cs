﻿using System;
using System.Text.RegularExpressions;

namespace NPL.M.A015.Exercise
{
    internal class Validate
    {
        // Hàm ValidateChoice dùng để ép người dùng nhập vào lựa chọn là kiểu số nguyên trong phạm vi yêu cầu
        public static int ValidateChoice(int min, int max)
        {
            while (true)
            {
                string input = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(input))
                    {
                        Console.Write("Lựa chọn không được trống! Vui lòng nhập một số: ");
                    }
                    else
                    {
                        int numericInput = int.Parse(input);
                        if (numericInput < min || numericInput > max)
                        {
                            Console.WriteLine("Số không hợp lệ! Vui lòng nhập một số từ " + min + " đến " + max);
                            Console.Write("Nhập một số từ " + min + " đến " + max + ": ");
                        }
                        else
                        {
                            return numericInput;
                        }
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Số không hợp lệ! Vui lòng nhập một số từ " + min + " đến " + max);
                    Console.Write("Nhập một số từ " + min + " đến " + max + ": ");
                }
            }
        }

        // Hàm ValidateNumber dùng để ép người dùng nhập vào số nguyên lớn hơn 0
        public static int ValidateNumber(string character)
        {
            while (true)
            {
                string input = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(input))
                    {
                        Console.Write(character + " không được trống! Vui lòng nhập một số: ");
                    }
                    else
                    {
                        int numericInput = int.Parse(input);
                        if (numericInput <= 0)
                        {
                            Console.WriteLine("Số không hợp lệ! " + character + " phải lớn hơn 0!");
                            Console.Write("Nhập một số hợp lệ: ");
                        }
                        else
                        {
                            return numericInput;
                        }
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Số không hợp lệ! " + character + " phải lớn hơn 0!");
                    Console.Write("Nhập một số hợp lệ: ");
                }
            }
        }

        // Hàm ValidateContinueChoice ép người dùng nhập vào chuỗi a hoặc b
        public static string ValidateContinueChoice(string a, string b)
        {
            while (true)
            {
                string choice = Console.ReadLine().ToLower();
                if (choice == a || choice == b)
                {
                    return choice;
                }
                else
                {
                    Console.WriteLine("Lựa chọn không hợp lệ! Vui lòng nhập lại");
                    Console.Write("Nhập lựa chọn của bạn (" + a + " hoặc " + b + "): ");
                }
            }
        }

        // Hàm ValidateName dùng để ép người dùng nhập vào chữ cái
        public static string ValidateName()
        {
            string pattern = @"^[A-Za-zÁ-ỹ\s]+$";
            while (true)
            {
                string name = Console.ReadLine();
                if (Regex.IsMatch(name, pattern))
                {
                    return name;
                }
                else
                {
                    Console.WriteLine("Tên không hợp lệ! Vui lòng chỉ nhập chữ cái!");
                    Console.Write("Nhập lại: ");
                }
            }
        }

        // Hàm ValidateDate dùng để ép người dùng nhập vào ngày sinh có định dạng dd/MM/yyyy
        public static DateOnly ValidateDate(string character)
        {
            while (true)
            {
                try
                {
                    string input = Console.ReadLine();
                    DateOnly inputDate = DateOnly.ParseExact(input, "dd/MM/yyyy", null);
                    return inputDate;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Ngày không hợp lệ! Vui lòng nhập " + character + " theo định dạng (dd/MM/yyyy)!");
                    Console.Write("Nhập " + character + " theo định dạng (dd/MM/yyyy): ");
                }
            }
        }
    }
}
