﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Department
    {
        public int departmentID { get; set; }
        public string departmentName { get; set; }
    }
}
