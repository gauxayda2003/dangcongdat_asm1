﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Employee
    {
        internal object departmentName;

        public int employeeID { get; set; }
        public string employeeName { get; set; }
        public int employeeAge { get; set; }
        public string employeeAdress { get; set; }
        public DateOnly hiredDate { get; set; }
        public bool status { get; set; }
        public int? departmentID { get; set; }
    }
}
