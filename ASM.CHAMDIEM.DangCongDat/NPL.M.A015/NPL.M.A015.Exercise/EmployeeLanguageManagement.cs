﻿using NPL.M.A015.Exercise;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{

    internal class EmployeeLanguageManagement
    {
        public List<Employee> employees = new List<Employee>();
        public List<Department> departments = new List<Department>();
        public List<ProgrammingLanguage> programmingLanguages = new List<ProgrammingLanguage>();
        public List<LanguageEmployee> languageemployees = new List<LanguageEmployee>();

        //Hàm Initial() khởi tạo dữ liệu ban đầu cho các list
        public void Initial()
        {
            employees.Add(new Employee { employeeID = 1, employeeName = "Đặng Công Đạt", employeeAge = 20, employeeAdress = "Bắc Ninh", hiredDate = DateOnly.Parse("2003/01/02"), status = true, departmentID = 1 });
            employees.Add(new Employee { employeeID = 2, employeeName = "Bùi Xuân Huấn", employeeAge = 20, employeeAdress = "Bắc Ninh", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 2 });
            employees.Add(new Employee { employeeID = 3, employeeName = "Nguyễn Thành Long", employeeAge = 20, employeeAdress = "Bắc Ninh", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 3 });
            employees.Add(new Employee { employeeID = 4, employeeName = "Dương Minh Tuyền", employeeAge = 20, employeeAdress = "Bắc Ninh", hiredDate = DateOnly.Parse("2003/01/02"), status = false, departmentID = 4 });
            employees.Add(new Employee { employeeID = 5, employeeName = "Ngô Bá Khá", employeeAge = 20, employeeAdress = "Bắc Ninh", hiredDate = DateOnly.Parse("2003/01/02"), status = true, departmentID = 5 });


            departments.Add(new Department { departmentID = 1, departmentName = "CNTT" });
            departments.Add(new Department { departmentID = 2, departmentName = "QTKD" });
            departments.Add(new Department { departmentID = 3, departmentName = "ATTT" });
            departments.Add(new Department { departmentID = 4, departmentName = "HCTC" });
            departments.Add(new Department { departmentID = 5, departmentName = "MOM" });

            programmingLanguages.Add(new ProgrammingLanguage { languageName = "C#", languageID = 1 });
            programmingLanguages.Add(new ProgrammingLanguage { languageName = "Java", languageID = 2 });
            programmingLanguages.Add(new ProgrammingLanguage { languageName = "Python", languageID = 3 });

            languageemployees.Add(new LanguageEmployee { employeeID = 1, languageID = 1 });
            languageemployees.Add(new LanguageEmployee { employeeID = 1, languageID = 2 });
            languageemployees.Add(new LanguageEmployee { employeeID = 3, languageID = 2 });
            languageemployees.Add(new LanguageEmployee { employeeID = 3, languageID = 3 });
        }

        //Hàm GetDepartMentWithMinimumEmployees cho phép người dùng nhập vào số nhân viên tối thiểu rồi trả về danh sách department có nhiều hơn hoặc bằng nhân viên
        public void GetDepartMentWithMinimumEmployees()
        {
            Console.Write("Nhập số lượng tối thiểu của nhân viên: ");
            int soLuongToiThieu = Validate.ValidateNumber("số lượng tối thiểu của nhân viên");
            GetDepartments(soLuongToiThieu);
            Console.Write("Bạn có muốn tiếp tục không? (Y/N): ");
            string luaChon = Validate.ValidateContinueChoice("Y","N");
            if (luaChon.ToLower().Equals("Y"))
            {
                GetDepartMentWithMinimumEmployees();
            }
        }


        //Hàm GetDepartments để in ra danh sách deparment có số nhân viên lớn hơn numberOfEmployees
        public void GetDepartments(int soLuongNhanVien)
        {
            var ketQuaPhongBan = from pb in departments
                                 join nv in employees on pb.departmentID equals nv.departmentID into ketQua
                                 where ketQua.Count() >= soLuongNhanVien
                                 select new
                                 {
                                     MaPhongBan = pb.departmentID,
                                     TenPhongBan = pb.departmentName,
                                     SoLuongNhanVien = ketQua.Count()
                                 };
            Console.WriteLine("Số phòng ban có số lượng nhân viên >= " + soLuongNhanVien + " nhân viên là: " + ketQuaPhongBan.Count() + "/" + departments.Count());
            if (ketQuaPhongBan.Count() > 0)
            {
                Console.WriteLine("Danh sách phòng ban có số lượng nhân viên >= " + soLuongNhanVien + " nhân viên:");
                Console.WriteLine("{0,-15} {1,-20} {2,-30}", "Mã Phòng Ban", "Tên Phòng Ban", "Số Lượng Nhân Viên");
                foreach (var phongBan in ketQuaPhongBan)
                {
                    Console.WriteLine("{0,-15} {1,-20} {2,-30}", phongBan.MaPhongBan, phongBan.TenPhongBan, phongBan.SoLuongNhanVien);
                }
            }
        }


        //Hàm GetEmployeesWorking in ra danh sach nhân viên đang làm việc 
        // Lấy danh sách những nhân viên đang làm việc
        public void GetEmployeesWorking()
        {
            var ketQuaNhanVien = from nv in employees
                                 where nv.status == true
                                 join pb in departments on nv.departmentID equals pb.departmentID
                                 select new
                                 {
                                     NhanVien = nv,
                                     TinhTrang = "Đang làm việc",
                                     PhongBan = pb
                                 };

            Console.WriteLine("Số lượng nhân viên đang làm việc: " + ketQuaNhanVien.Count() + "/" + employees.Count());
            if (ketQuaNhanVien.Count() > 0)
            {
                Console.WriteLine("Danh sách nhân viên đang làm việc: ");
                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", "Mã nhân viên", "Tên nhân viên", "Tuổi", "Địa chỉ", "Ngày tuyển dụng", "Phòng ban", "Tình trạng");
                foreach (var e in ketQuaNhanVien)
                {
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", e.NhanVien.employeeID, e.NhanVien.employeeName, e.NhanVien.employeeAge, e.NhanVien.employeeAdress, e.NhanVien.hiredDate, e.PhongBan.departmentName, e.TinhTrang);
                }
            }
        }

        // Lấy danh sách nhân viên biết ngôn ngữ languageName do người dùng nhập vào
        public void GetEmployees(string languageName)
        {
            var ketQuaNhanVien = from emla in languageemployees
                                 join nv in employees on emla.employeeID equals nv.employeeID
                                 join pb in departments on nv.departmentID equals pb.departmentID
                                 join lg in programmingLanguages on emla.languageID equals lg.languageID
                                 where lg.languageName.ToLower() == languageName.ToLower()
                                 select new
                                 {
                                     NhanVien = nv,
                                     TenNgonNgu = emla,
                                     PhongBan = pb,
                                     TinhTrang = (nv.status) ? "Đang làm việc" : "Không làm việc"
                                 };

            Console.WriteLine("Số lượng nhân viên biết " + languageName + " là: " + ketQuaNhanVien.Count() + "/" + employees.Count());
            if (ketQuaNhanVien.Count() > 0)
            {
                Console.WriteLine("Danh sách nhân viên biết " + languageName + ": ");
                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", "Mã nhân viên", "Tên nhân viên", "Tuổi", "Địa chỉ", "Ngày tuyển dụng", "Phòng ban", "Tình trạng");
                foreach (var e in ketQuaNhanVien)
                {
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -12} {6,-15}", e.NhanVien.employeeID, e.NhanVien.employeeName, e.NhanVien.employeeAge, e.NhanVien.employeeAdress, e.NhanVien.hiredDate, e.NhanVien.departmentName, e.TinhTrang);
                }
            }
        }


        //Hàm GetEmployeeByLanguage cho phép người dùng nhập vào ngôn ngữ lập trình và in ra danh sách nhân viên biết ngôn ngữ đó

        // Lấy danh sách nhân viên theo ngôn ngữ lập trình
        public void GetEmployeeByLanguage()
        {
            bool canContinue = true;

            while (canContinue)
            {
                Console.Write("Nhập tên ngôn ngữ lập trình: ");
                string languageName = Console.ReadLine().ToLower();
                int count = (from lang in programmingLanguages
                             where lang.languageName.ToLower() == languageName
                             select lang).Count();

                if (count <= 0)
                {
                    Console.WriteLine(languageName + " không tồn tại trong danh sách ngôn ngữ lập trình!\nVui lòng nhập lại!");
                }
                else
                {
                    GetEmployees(languageName);
                    canContinue = false;
                }
            }

            Console.Write("Bạn có muốn tiếp tục? (Y/N): ");
            string option = Validate.ValidateContinueChoice("Y","N");
            if (option.ToLower().Equals("Y"))
            {
                GetEmployeeByLanguage();
            }
        }

        // Lấy danh sách ngôn ngữ mà nhân viên có ID do người dùng nhập biết
        public void GetLanguageWithEmployeeID(int employeeID)
        {
            var languageResult = from lanemp in languageemployees
                                 join emp in employees on lanemp.employeeID equals emp.employeeID
                                 join lang in programmingLanguages on lanemp.languageID equals lang.languageID
                                 where emp.employeeID == employeeID
                                 select lang;

            Console.WriteLine("Số lượng ngôn ngữ mà nhân viên có ID = " + employeeID + " biết: " + languageResult.Count());
            if (languageResult.Count() > 0)
            {
                Console.WriteLine("Danh sách ngôn ngữ mà nhân viên có ID = " + employeeID + " biết: ");
                Console.WriteLine("{0,-13} {1,-20}", "STT", "Ngôn ngữ lập trình");
                int i = 1;
                foreach (var e in languageResult)
                {
                    Console.WriteLine("{0,-13} {1,-20}", i, e.languageName);
                    i++;
                }
            }
        }

        // Lấy danh sách nhân viên có nhiều ngôn ngữ lập trình biết
        public void GetSeniorEmployee()
        {
            var employeeResult = from emp in employees
                                 join emlan in languageemployees on emp.employeeID equals emlan.employeeID into result
                                 where result.Count() > 1
                                 select new
                                 {
                                     employee = emp,
                                     numOflanguage = result.Count(),
                                     status = (emp.status == true) ? "Đang làm việc" : "Không làm việc"
                                 };

            Console.WriteLine("Số lượng nhân viên biết nhiều ngôn ngữ lập trình: " + employeeResult.Count() + "/" + employees.Count());
            if (employeeResult.Count() > 0)
            {
                Console.WriteLine("Danh sách nhân viên biết nhiều ngôn ngữ lập trình: ");
                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} {6,-15}", "Mã nhân viên", "Tên nhân viên", "Tuổi", "Địa chỉ", "Ngày tuyển dụng", "Số lượng ngôn ngữ", "Tình trạng");
                foreach (var e in employeeResult)
                {
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} {6,-15}", e.employee.employeeID, e.employee.employeeName, e.employee.employeeAge, e.employee.employeeAdress, e.employee.hiredDate, e.numOflanguage, e.status);
                }
            }
        }

        //Hàm GetEmployeePaging Phân trang danh sách trước, lọc kết quả theo tên rồi trả về về danh sách nhân viên được sắp xếp
        // Lấy danh sách nhân viên với phân trang
        public void GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            var query = from emp in employees
                        select emp;

            // Lấy trang dữ liệu dựa trên pageSize và pageIndex
            var pagingResult = query.Skip(pageSize * (pageIndex - 1)).Take(pageSize);

            if (pagingResult.Count() > 0)
            {
                var filteredResult = pagingResult;

                if (!string.IsNullOrEmpty(employeeName))
                {
                    // Lọc theo tên nhân viên nếu tên không rỗng
                    filteredResult = pagingResult.Where(emp => emp.employeeName.Contains(employeeName));
                }

                var orderedResult = order.ToLower() == "asc"
                    ? filteredResult.OrderBy(emp => emp.employeeName)
                    : filteredResult.OrderByDescending(emp => emp.employeeName);

                Console.WriteLine("Danh sách nhân viên ở trang số " + pageIndex + " với kích thước trang " + pageSize + " là: ");
                Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20}", "Mã nhân viên", "Tên nhân viên", "Tuổi", "Địa chỉ", "Ngày tuyển dụng", "Tình trạng");

                foreach (var e in orderedResult)
                {
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} ", e.employeeID, e.employeeName, e.employeeAge, e.employeeAdress, e.hiredDate, e.status ? "Đang làm việc" : "Không làm việc");
                }
            }
            else
            {
                Console.WriteLine("Danh sách nhân viên không tồn tại vì vượt quá phạm vi trang!");
            }
        }

        // Hàm GetEmployeByPaging cho phép người dùng nhập kích thước trang, trang, tên nhân viên, và thứ tự sắp xếp
        public void GetEmployeByPaging()
        {
            Console.Write("Nhập kích thước trang: ");
            int pageSize = Validate.ValidateNumber("Kích thước trang");
            Console.Write("Nhập số trang: ");
            int pageIndex = Validate.ValidateNumber("Số trang");
            Console.Write("Nhập tên nhân viên: ");
            string employeeName = Console.ReadLine();
            Console.Write("Sắp xếp theo thứ tự tăng dần hoặc giảm dần? (ASC hoặc DESC): ");
            string order = Validate.ValidateContinueChoice("ASC", "DESC");

            GetEmployeePaging(pageIndex, pageSize, employeeName, order);

            Console.Write("Bạn có muốn tiếp tục? (Y/N): ");
            string option = Validate.ValidateContinueChoice("Y","N");
            if (option.ToLower() == "Y")
            {
                GetEmployeByPaging();
            }
        }

        //Hàm GetDepartments để in ra danh sách tất cả department và nhân ciên của nó
        // Lấy danh sách tất cả các phòng ban và các nhân viên thuộc phòng ban
        public void GetDepartments()
        {
            Console.WriteLine("Danh sách tất cả các phòng ban và danh sách nhân viên thuộc mỗi phòng ban");
            foreach (Department department in departments)
            {
                var employeesInDepartment = employees.Where(emp => emp.departmentID == department.departmentID);

                Console.WriteLine("Tên phòng ban: " + department.departmentName);
                Console.WriteLine("Số nhân viên thuộc phòng ban " + department.departmentName + " là: " + employeesInDepartment.Count() + "/" + employees.Count());

                if (employeesInDepartment.Count() > 0)
                {
                    Console.WriteLine("Danh sách nhân viên thuộc phòng ban " + department.departmentName);
                    Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20}", "Mã nhân viên", "Tên nhân viên", "Tuổi", "Địa chỉ", "Ngày tuyển dụng", "Tình trạng");
                    foreach (var employee in employeesInDepartment)
                    {
                        Console.WriteLine("{0,-13} {1,-20} {2,-15} {3,-20} {4,-15} {5, -20} ", employee.employeeID, employee.employeeName, employee.employeeAge, employee.employeeAdress, employee.hiredDate, employee.status ? "Đang làm việc" : "Không làm việc");
                    }
                }
                Console.WriteLine();
            }
        }

        // Hàm AddEmployee để nhập thông tin nhân viên
        public void AddEmployee()
        {
            Console.WriteLine("Nhập thông tin nhân viên:");
            Employee employee = new Employee();
            Console.Write("Nhập Mã nhân viên: ");
            int employeeID = 0;
            bool duplicateID = true;
            while (duplicateID)
            {
                employeeID = Validate.ValidateNumber("Mã nhân viên");
                int countID = employees.Count(e => e.employeeID == employeeID);
                if (countID > 0)
                {
                    Console.WriteLine("Mã nhân viên đã tồn tại! Vui lòng nhập lại!");
                    Console.Write("Nhập Mã nhân viên: ");
                }
                else
                    duplicateID = false;
            }
            employee.employeeID = employeeID;

            Console.Write("Nhập Tên nhân viên: ");
            employee.employeeName = Validate.ValidateName();

            Console.Write("Nhập Tuổi nhân viên: ");
            employee.employeeAge = Validate.ValidateNumber("Tuổi nhân viên");

            Console.Write("Nhập Địa chỉ nhân viên: ");
            employee.employeeAdress = Console.ReadLine();

            Console.Write("Nhập Ngày tuyển dụng (dd/MM/yyyy): ");
            employee.hiredDate = Validate.ValidateDate("Ngày tuyển dụng");

            Console.WriteLine("Chọn Tình trạng nhân viên: ");
            Console.WriteLine("1. Đang làm việc");
            Console.WriteLine("2. Không làm việc");
            Console.Write("Tình trạng của nhân viên (Nhập số 1 hoặc 2): ");
            int statusChoice = Validate.ValidateChoice(1, 2);
            employee.status = statusChoice == 1;

            Console.WriteLine("Chọn Mã phòng ban cho nhân viên: ");
            foreach (Department department in departments)
            {
                Console.WriteLine(department.departmentID + ". " + department.departmentName);
            }
            Console.Write("Mã phòng ban của nhân viên (Nhập số): ");
            int departmentID = 0;
            bool validDepartmentID = true;
            while (validDepartmentID)
            {
                departmentID = Validate.ValidateNumber("Mã phòng ban");
                int count = departments.Count(dept => dept.departmentID == departmentID);
                if (count <= 0)
                {
                    Console.WriteLine("Mã phòng ban không hợp lệ. Vui lòng chọn lại phòng ban của nhân viên!");
                    Console.Write("Mã phòng ban của nhân viên (Nhập số): ");
                }
                else
                    validDepartmentID = false;
            }
            employee.departmentID = departmentID;

            employees.Add(employee);
            Console.WriteLine("Thông tin nhân viên đã được thêm!");
            Console.Write("Bạn có muốn tiếp tục? (Y/N): ");
            string option = Validate.ValidateContinueChoice("Y", "N");
            if (option.ToLower() == "Y")
            {
                AddEmployee();
            }
        }

        // Hàm AddProgramLanguage để nhập thông tin của ngôn ngữ lập trình
        public void AddProgramLanguage()
        {
            Console.WriteLine("Nhập thông tin ngôn ngữ lập trình:");
            Console.Write("Nhập Mã ngôn ngữ lập trình: ");
            ProgrammingLanguage language = new ProgrammingLanguage();
            int languageID = 0;
            bool duplicateID = true;
            while (duplicateID)
            {
                languageID = Validate.ValidateNumber("Mã ngôn ngữ lập trình");
                int countID = programmingLanguages.Count(lang => lang.languageID == languageID);
                if (countID > 0)
                {
                    Console.WriteLine("Mã ngôn ngữ lập trình đã tồn tại! Vui lòng nhập lại!");
                    Console.Write("Nhập Mã ngôn ngữ lập trình: ");
                }
                else
                    duplicateID = false;
            }
            language.languageID = languageID;

            Console.Write("Nhập Tên ngôn ngữ lập trình: ");
            string languageName = null;
            bool duplicateName = true;
            while (duplicateName)
            {
                languageName = Console.ReadLine();
                int count = programmingLanguages.Count(lang => lang.languageName.ToLower() == languageName.ToLower());
                if (count > 0)
                {
                    Console.WriteLine("Tên ngôn ngữ lập trình đã tồn tại! Vui lòng nhập lại!");
                    Console.Write("Nhập Tên ngôn ngữ lập trình: ");
                }
                else
                    duplicateName = false;
            }
            language.languageName = languageName;

            programmingLanguages.Add(language);
            Console.WriteLine("Thông tin ngôn ngữ lập trình đã được thêm!");

            Console.Write("Bạn có muốn tiếp tục? (Y/N): ");
            string option = Validate.ValidateContinueChoice("Y","N");
            if (option.ToLower() == "Y")
            {
                AddProgramLanguage();
            }
        }

        // Hàm AddDepartment để nhập thông tin phòng ban
        public void AddDepartment()
        {
            Console.WriteLine("Nhập thông tin phòng ban:");
            Console.Write("Nhập Mã phòng ban: ");
            Department department = new Department();
            int departmentID = 0;
            bool duplicateID = true;
            while (duplicateID)
            {
                departmentID = Validate.ValidateNumber("Mã phòng ban");
                int countID = departments.Count(dept => dept.departmentID == departmentID);
                if (countID > 0)
                {
                    Console.WriteLine("Mã phòng ban đã tồn tại! Vui lòng nhập lại!");
                    Console.Write("Nhập Mã phòng ban: ");
                }
                else
                    duplicateID = false;
            }
            department.departmentID = departmentID;

            Console.Write("Nhập Tên phòng ban: ");
            string departmentName = null;
            bool duplicateName = true;
            while (duplicateName)
            {
                departmentName = Console.ReadLine();
                int count = departments.Count(dept => dept.departmentName.ToLower() == departmentName.ToLower());
                if (count > 0)
                {
                    Console.WriteLine("Tên phòng ban đã tồn tại! Vui lòng nhập lại!");
                    Console.Write("Nhập Tên phòng ban: ");
                }
                else
                    duplicateName = false;
            }
            department.departmentName = departmentName;

            departments.Add(department);
            Console.WriteLine("Thông tin phòng ban đã được thêm!");

            Console.Write("Bạn có muốn tiếp tục? (Y/N): ");
            string option = Validate.ValidateContinueChoice("Y","N");
            if (option.ToLower() == "Y")
            {
                AddDepartment();
            }
        }

        //Hàm AddSkill cho phép người dùng nhập thông tin employeeID biết ngôn ngữ languageID
        // Hàm AddSkill cho phép người dùng thêm kỹ năng (ngôn ngữ lập trình) cho một nhân viên
        public void AddSkill()
        {
            Console.WriteLine("Nhập thông tin kỹ năng (ngôn ngữ lập trình):");
            LanguageEmployee skill = new LanguageEmployee();

            Console.Write("Nhập Mã nhân viên: ");
            int employeeID = 0;
            bool validEmployeeID = false;

            // Kiểm tra tính hợp lệ của Mã nhân viên
            while (!validEmployeeID)
            {
                employeeID = Validate.ValidateNumber("Mã nhân viên");
                int countID = employees.Count(emp => emp.employeeID == employeeID);

                if (countID <= 0)
                {
                    Console.WriteLine("Mã nhân viên không tồn tại! Vui lòng nhập lại!");
                    Console.Write("Nhập Mã nhân viên: ");
                }
                else
                    validEmployeeID = true;
            }

            Console.Write("Nhập Mã ngôn ngữ lập trình: ");
            int languageID = 0;
            bool validLanguageID = false;

            // Kiểm tra tính hợp lệ của Mã ngôn ngữ lập trình
            while (!validLanguageID)
            {
                languageID = Validate.ValidateNumber("Mã ngôn ngữ lập trình");
                int countID = programmingLanguages.Count(lang => lang.languageID == languageID);

                if (countID <= 0)
                {
                    Console.WriteLine("Mã ngôn ngữ lập trình không tồn tại! Vui lòng nhập lại!");
                    Console.Write("Nhập Mã ngôn ngữ lập trình: ");
                }
                else
                    validLanguageID = true;
            }

            var existingSkill = languageemployees.Where(emla => emla.employeeID == employeeID && emla.languageID == languageID);

            // Kiểm tra xem nhân viên đã biết ngôn ngữ lập trình này chưa
            if (existingSkill.Count() > 0)
            {
                Console.WriteLine("Nhân viên đã biết ngôn ngữ lập trình này!");
            }
            else
            {
                skill.employeeID = employeeID;
                skill.languageID = languageID;
                languageemployees.Add(skill);
                Console.WriteLine("Thông tin Nhân viên và Ngôn ngữ lập trình đã được thêm!");
            }

            Console.Write("Bạn có muốn tiếp tục? (Y/N): ");
            string option = Validate.ValidateContinueChoice("Y","N");

            if (option.ToLower() == "Y")
            {
                AddSkill();
            }
        }

        internal void GetLanguageWithEmployeeID()
        {
            throw new NotImplementedException();
        }
    }
}


