﻿using NPL.M.A015.Exercise;
using System.ComponentModel;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

EmployeeLanguageManagement management = new EmployeeLanguageManagement();
management.Initial();


int choice;

do
{
    Console.WriteLine("1. Nhập thông tin Nhân viên mới");
    Console.WriteLine("2. Nhập thông tin Ngôn ngữ lập trình mới");
    Console.WriteLine("3. Nhập thông tin Phòng ban mới");
    Console.WriteLine("4. Nhập thông tin Nhân viên và Ngôn ngữ lập trình mới");
    Console.WriteLine("5. Xem Phòng ban có ít nhân viên nhất");
    Console.WriteLine("6. Xem Nhân viên đang làm việc");
    Console.WriteLine("7. Xem Nhân viên theo Ngôn ngữ lập trình");
    Console.WriteLine("8. Xem Ngôn ngữ lập trình theo Mã Nhân viên");
    Console.WriteLine("9. Xem Nhân viên nắm nhiều Ngôn ngữ lập trình nhất");
    Console.WriteLine("10. Xem Nhân viên với phân trang và sắp xếp");
    Console.WriteLine("11. Xem danh sách Phòng ban với Nhân viên thuộc mỗi Phòng ban");
    Console.WriteLine("0. Thoát");
    Console.Write("Nhập số tương ứng với lựa chọn: ");
    choice = Validate.ValidateChoice(0, 11);

    switch (choice)
    {
        case 1:
            management.AddEmployee();
            break;

        case 2:
            management.AddProgramLanguage();
            break;

        case 3:
            management.AddDepartment();
            break;

        case 4:
            management.AddSkill();
            break;

        case 5:
            management.GetDepartMentWithMinimumEmployees();
            break;

        case 6:
            management.GetEmployeesWorking();
            break;

        case 7:
            management.GetEmployeeByLanguage();
            break;

        case 8:
            management.GetLanguageWithEmployeeID();
            break;

        case 9:
            management.GetSeniorEmployee();
            break;

        case 10:
            management.GetEmployeByPaging();
            break;

        case 11:
            management.GetDepartments();
            break;

        case 0:
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng nhập số từ 1 đến 11.");
            break;
    }
}
while (choice > 0 && choice <= 11);

Console.ReadKey();
