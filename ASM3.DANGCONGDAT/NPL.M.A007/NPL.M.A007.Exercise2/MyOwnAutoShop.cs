﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
        Sedan sedan1 = new Sedan(120, 10000, "Red", 60);
        Ford ford1 = new Ford(110, 10000, "Blue", 2020, 1000);
        Ford ford2 = new Ford(120, 10000, "Violet", 2023, 1500);
        Truck truck1 = new Truck(160, 10000, "Black", 4000);
        Truck truck2 = new Truck(180, 10000, "Sliver", 10000);

        public void DisplayPriceofCars()
        {
            sedan1.DisplayPrice();
            ford1.DisplayPrice();
            ford2.DisplayPrice();
            truck1.DisplayPrice();
            truck2.DisplayPrice();
        }
    }
}
