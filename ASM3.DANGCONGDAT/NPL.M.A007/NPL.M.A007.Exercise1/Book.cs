﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise1
{
    internal class Book
    {
        public string? BookName { get; set; }
        public int Isbn { get; set; }
        public string? AuthorName { get; set; }
        public string? PublisherName { get; set; }

        public Book()
        {
        }

        public Book(string? bookName, int isbn, string? authorName, string? publisherName)
        {
            this.BookName = bookName;
            this.Isbn = isbn;
            this.AuthorName = authorName;
            this.PublisherName = publisherName;
        }

        public string GetBookInformation()
        {
            String s = String.Format("{0,-20} {1,-20} {2,-20} {3,-20}\n", "ISBN Number", "Book Name", "Author Name", "Publisher Namer");


            s += String.Format("{0,-20} {1,-20} {2,-20} {3,-20}\n", BookName, Isbn, AuthorName, PublisherName);



            return s;

        }

    }
}
